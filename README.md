## Final Project

## Kelompok 7

## Anggota Kelompok
Rinaldi Akbar Setiawan<br>
Yunadi Fitra Munawar<br>
Rahmad setiadi

## Tema Project
E-Voting

## ERD
![img](erd voting.png)


## Link Video
https://drive.google.com/file/d/1gbyJ8UX7QlyEwHjf3y0BPrDFDNBZF050

## Link Project (Hosting)
https://rinaldiakbarsetiawan.com/