<?php

namespace App\Http\Controllers;

use App\Kategori;
use App\Pemilu;
use Illuminate\Http\Request;
use DB;
use File;

class PemiluController extends Controller
{
    public function index()
    {
		$pemilu = Pemilu::all();
		$kategori = DB::table('kategori')->get();
		return view('pemilu.index', compact('pemilu','kategori'));
	}
	
	public function create()
    {
		$kategori = DB::table('kategori')->get();
        return view('pemilu.create', compact('kategori'));
    }
	
	public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg',
			'kategori_id' => 'required'
        ]);
		
		$filename=time().'.'.$request->foto->extension();
		$request->foto->move(public_path('data_file'), $filename);
		
		$pemilu = new Pemilu();
		$pemilu->nama = $request->nama;
		$pemilu->foto = $filename;
		$pemilu->kategori_id = $request->kategori_id;
		
		$pemilu->save();
		
		return redirect('/pemilu');
    }
	
	public function edit($id)
    {
		$pemilu = Pemilu::findOrFail($id);
		$kategori = DB::table('kategori')->get();
        return view('pemilu.edit', compact('pemilu','kategori'));
    }
	
	public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'foto' => 'image|mimes:jpeg,png,jpg',
			'kategori_id' => 'required'
        ]);
		
		$pemilu=Pemilu::find($id);
		$pemilu->nama = $request->nama;
		$pemilu->kategori_id = $request->kategori_id;
		
		if($request->has('foto'))
		{
			$path="data_file/";
			File::delete($path.$pemilu->foto);
			$filename=time().'.'.$request->foto->extension();
			$request->foto->move(public_path('data_file'), $filename);
			$pemilu->foto = $filename;
		}
		
		$pemilu->save();
		
		return redirect('/pemilu');
    }
	
	public function destroy($id)
	{
		$pemilu=Pemilu::find($id);
		$path="data_file/";
		File::delete($path.$pemilu->foto);
		$pemilu->delete();
		return redirect('/pemilu');
	}
}
