<?php

namespace App\Http\Controllers;

use Auth;
use App\Profile;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
	{
		$profile = Profile::find(Auth::user()->id);
		return view('profile.edit', compact('profile'));
	}

    public function edit($id)
	{
		$profile = Profile::find($id);
		return view('profile.edit', compact('profile'));
	}
	
	public function update(Request $request, $id)
    {
        $request->validate([
            'alamat' => 'required',
            'umur' => 'required',
			'jenis_kelamin' => 'required'
        ]);
        $profile=Profile::find($id);
		$profile->alamat = $request->alamat;
		$profile->umur = $request->umur;
		$profile->jenis_kelamin = $request->jenis_kelamin;
        $profile->save();
        return redirect('/profile/'.$id.'/edit');
    }

	protected function create()
    {
        $datax=[
			['name'=>'Febyani Rachim','email'=>'Febyani_Rachim@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Fikhri Masri','email'=>'Fikhri_Masri@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Gabriella Margareth H','email'=>'Gabriella_Margareth_H@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Gede Narendra Saguna','email'=>'Gede_Narendra_Saguna@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Hafis Alrafi Irsal','email'=>'Hafis_Alrafi_Irsal@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Haposan Nico Pardamean Sitorus','email'=>'Haposan_Nico_Pardamean_Sitorus@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Hardiana','email'=>'Hardiana@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Hilmi Cahya Rinardi','email'=>'Hilmi_Cahya_Rinardi@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Hutama Pra Raka Atmaja','email'=>'Hutama_Pra_Raka_Atmaja@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Ilham Nur Hakim','email'=>'Ilham_Nur_Hakim@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Indah Permata Syahnan','email'=>'Indah_Permata_Syahnan@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Indira Dara Safira','email'=>'Indira_Dara_Safira@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Indra Haryo Dewanto','email'=>'Indra_Haryo_Dewanto@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Irsyad Muhammad','email'=>'Irsyad_Muhammad@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Iyon Priyono','email'=>'Iyon_Priyono@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Januari Arka','email'=>'Januari_Arka@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Jinan Rhsi Rahsyaningtwas Jannata','email'=>'Jinan_Rhsi_Rahsyaningtwas_Jannata@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Jorgie Muhammad Indrastiadi','email'=>'Jorgie_Muhammad_Indrastiadi@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Kartiko Fajar Gumilang','email'=>'Kartiko_Fajar_Gumilang@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Kurniawan Aditya Utama','email'=>'Kurniawan_Aditya_Utama@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'M. Alief Hidayah Baso','email'=>'M._Alief_Hidayah_Baso@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Masyithah Nur Aulia','email'=>'Masyithah_Nur_Aulia@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Maya Monita Larasati','email'=>'Maya_Monita_Larasati@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Mohamad Sony Marsono','email'=>'Mohamad_Sony_Marsono@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Mohammad Firda Ari Sanjaya','email'=>'Mohammad_Firda_Ari_Sanjaya@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Muhammad Al Bahri','email'=>'Muhammad_Al_Bahri@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Muhammad Arfidh','email'=>'Muhammad_Arfidh@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Muhammad Fadly','email'=>'Muhammad_Fadly@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Muhammad Farhan Amanda','email'=>'Muhammad_Farhan_Amanda@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Muhammad Ikhsan Kurniawan','email'=>'Muhammad_Ikhsan_Kurniawan@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Muhammad Rayhan Ramadhan','email'=>'Muhammad_Rayhan_Ramadhan@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Muhammad Rifky Putra Ananda','email'=>'Muhammad_Rifky_Putra_Ananda@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Muhammad Rifqi','email'=>'Muhammad_Rifqi@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Muhammad Rifqi Hafidh','email'=>'Muhammad_Rifqi_Hafidh@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Mustabiqul Khair','email'=>'Mustabiqul_Khair@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Mutia Nurazizah Rachmawati','email'=>'Mutia_Nurazizah_Rachmawati@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Nabila Mauliddina','email'=>'Nabila_Mauliddina@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Nadia Ufairah','email'=>'Nadia_Ufairah@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Nunun Abdurrahman','email'=>'Nunun_Abdurrahman@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Oktey Ikkova Pasha','email'=>'Oktey_Ikkova_Pasha@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Patih Fathin Rizal','email'=>'Patih_Fathin_Rizal@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Pradipta Kurniawan','email'=>'Pradipta_Kurniawan@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'R. Hatim Muhammad Amin','email'=>'R._Hatim_Muhammad_Amin@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Rabbi Fijar Mayoza','email'=>'Rabbi_Fijar_Mayoza@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Rahma Salsabila Sekar Cahyaningrum','email'=>'Rahma_Salsabila_Sekar_Cahyaningrum@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Rani Sari Murti','email'=>'Rani_Sari_Murti@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Ratu Hasanah Tambunan','email'=>'Ratu_Hasanah_Tambunan@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Raufah Melvidya Lubis','email'=>'Raufah_Melvidya_Lubis@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Rayiemas Manggala Putra','email'=>'Rayiemas_Manggala_Putra@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Regita Yustania Esyaganitha','email'=>'Regita_Yustania_Esyaganitha@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Renata Christy','email'=>'Renata_Christy@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Restu Nursobah','email'=>'Restu_Nursobah@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Reza Satria Akhdani','email'=>'Reza_Satria_Akhdani@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Richa Amalia Permatasari','email'=>'Richa_Amalia_Permatasari@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih'],
['name'=>'Rifqi Mukti Wicaksana','email'=>'Rifqi_Mukti_Wicaksana@gmail.com','password'=>'123456abc','alamat'=>'bandung','umur'=>'30','jenis_kelamin'=>'laki_laki','peran'=>'pemilih']
		];

		foreach($datax as $data)
		{
			$user =  User::create([
				'name' => $data['name'],
				'email' => $data['email'],
				'password' => Hash::make($data['password']),
			]);
			
			Profile::create([
			'alamat' => $data['alamat'],
			'umur' => $data['umur'],
			'jenis_kelamin' => $data['jenis_kelamin'],
			'peran' => "pemilih",
			'user_id' => $user->id
			]);
		}
		return $user;
    }
}
