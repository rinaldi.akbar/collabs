<?php

namespace App\Http\Controllers;

use DB;
use App\Voting;
use Auth;
use Illuminate\Http\Request;

class VotingController extends Controller
{
    public function vote($id)
    {
		$pemilu = DB::table('pemilu')->where('kategori_id', $id)->get();
		$kategori = DB::table('kategori')->where('id', $id)->first();
		return view('voting.create', compact('pemilu','kategori'));
    }
	
	public function store(Request $request)
    {
        $voting = new Voting();
		$voting->user_id = Auth::user()->id;
		$voting->pemilu_id = $request->pemilu_id;
		$voting->voting = 1;
		$kategori = $request->kategori;
		$next_kategori=intval($kategori)+1;
		$voting->save();
		if($next_kategori>5) return view('voting.penutup');
		else return redirect('/voting/'.$next_kategori.'/vote');
    }

	public function rekapitulasi()
	{
		$kategori = DB::table('kategori')->get();
		$result = DB::table('voting')
					->join('pemilu', 'pemilu_id','=','pemilu.id')
					->join('kategori', 'kategori_id','=','kategori.id')
					//->select('pemilu.nama', 'pemilu.kategori_id', 'kategori.deskripsi')
					->select(DB::raw('sum(voting) as jumlah_suara, pemilu.nama, pemilu.kategori_id, kategori.deskripsi'))
					->groupBy('pemilu.nama', 'pemilu.kategori_id', 'kategori.deskripsi')
					->orderBy('pemilu.kategori_id', 'asc')
					->get();
		//dd($result);
		return view('voting.rekapin', compact('result','kategori'));
	}
}
