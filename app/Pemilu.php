<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemilu extends Model
{
    protected $table = "pemilu";
    protected $fillable = ["nama","foto","kategori_id"];
	
	public function kategori()
	{
		return $this->belongsTo('App\Kategori');
	}
}
