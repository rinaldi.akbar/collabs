<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voting extends Model
{
    protected $table = "voting";
    protected $fillable = ["user_id","pemilu_id","voting"];
}
