@extends('app')

@section('content')
<div class="card">
	<div class="card-header">
		<h3 class="card-title">Tambah Kategori</h3>

		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
				<i class="fas fa-minus"></i>
			</button>
			<button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
				<i class="fas fa-times"></i>
			</button>
		</div>
	</div>
	<div class="card-body">
<div>
	<form action="/kategori" method="POST">
		@csrf
		<div class="form-group">
			<label for="nama">Nama</label>
			<input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
			@error('nama')
				<div class="alert alert-danger">
					{{ $message }}
				</div>
			@enderror
		</div>
		<div class="form-group">
			<label for="deskripsi">Deskripsi</label>
			<input type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan Deskripsi">
			@error('deskripsi')
				<div class="alert alert-danger">
					{{ $message }}
				</div>
			@enderror
		</div>
		<button type="submit" class="btn btn-primary">Tambah</button>
	</form>
</div>
</div>
    <!-- /.card-body -->
    <div class="card-footer">
        Final Project 1
    </div>
    <!-- /.card-footer-->
</div>
@endsection