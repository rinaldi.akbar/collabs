@extends('app')

@section('content')
<div class="card">
	<div class="card-header">
		<h3 class="card-title">Tambah Pemilu</h3>

		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
				<i class="fas fa-minus"></i>
			</button>
			<button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
				<i class="fas fa-times"></i>
			</button>
		</div>
	</div>
	<div class="card-body">
		<div>
			<form action="/pemilu" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="nama">Nama</label>
					<input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
					@error('nama')
					<div class="alert alert-danger">
						{{ $message }}
					</div>
					@enderror
				</div>
				<div class="form-group">
					<label for="foto">Foto</label>
					<input type="file" class="form-control" name="foto" id="foto">
					@error('foto')
					<div class="alert alert-danger">
						{{ $message }}
					</div>
					@enderror
				</div>
				<div class="form-group">
					<label for="kategori_id">Kategori</label>
					<select class="form-control" name="kategori_id" id="kategori_id">
						<option value="">--- Pilih Kategori ---</option>
						@foreach($kategori as $item)
						<option value="{{$item->id}}">{{$item->deskripsi}}</option>
						@endforeach
					</select>
					@error('kategori_id')
					<div class="alert alert-danger">
						{{ $message }}
					</div>
					@enderror
				</div>
				<button type="submit" class="btn btn-primary">Tambah</button>
			</form>
		</div>
	</div>
	<!-- /.card-body -->
	<div class="card-footer">
		Final Project 1
	</div>
	<!-- /.card-footer-->
</div>
@endsection