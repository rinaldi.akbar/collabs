@extends('app')

@section('title')
Edit Profile
@endsection

@section('content')
<div class="card">
	<div class="card-header">
		<h3 class="card-title">Edit Profile</h3>

		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
				<i class="fas fa-minus"></i>
			</button>
			<button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
				<i class="fas fa-times"></i>
			</button>
		</div>
	</div>
	<div class="card-body">
		<div>
			<form action="/profile/{{$profile->id}}" method="POST">
				@csrf
				@method('PUT')
				<div class="form-group">
					<label for="nama">Nama</label>
					<input type="text" class="form-control" name="nama" id="nama" value="{{Auth::user()->name}}" disabled>
				</div>
				<div class="form-group">
					<label for="alamat">Alamat</label>
					<textarea class="form-control" name="alamat" id="alamat">{{$profile->alamat}}</textarea>
					@error('alamat')
					<div class="alert alert-danger">
						{{ $message }}
					</div>
					@enderror
				</div>
				<div class="form-group">
					<label for="umur">Umur</label>
					<input type="number" class="form-control" name="umur" id="umur" value="{{$profile->umur}}">
					@error('umur')
					<div class="alert alert-danger">
						{{ $message }}
					</div>
					@enderror
				</div>
				<div class="form-group">
					<label for="bio">Jenis Kelamin</label>
					<select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
						<option value="">--- Pilih Jenis Kelamin ---</option>
						@if($profile->jenis_kelamin=="Laki-Laki")
						<option value="laki_laki" selected>Laki-Laki</option>
						<option value="perempuan">Perempuan</option>
						@else
						<option value="laki_laki">Laki-Laki</option>
						<option value="perempuan" selected>Perempuan</option>
						@endif
					</select>
					@error('bio')
					<div class="alert alert-danger">
						{{ $message }}
					</div>
					@enderror
				</div>
				<button type="submit" class="btn btn-primary">Edit</button>
			</form>
		</div>
	</div>
	<!-- /.card-body -->
	<div class="card-footer">
		Final Project 1
	</div>
	<!-- /.card-footer-->
</div>
@endsection