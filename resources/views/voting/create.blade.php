<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Form</title>
</head>
<style type="text/css">
    .row.heading h2 {
        color: rgb(3, 3, 3);
        font-size: 52.52px;
        line-height: 95px;
        font-weight: 400;
        text-align: center;
        margin: 0 0 40px;
        padding-bottom: 20px;
        text-transform: uppercase;
    }

    ul {
        margin: 0;
        padding: 0;
        list-style: none;
    }

    .heading.heading-icon {
        display: block;
    }

    .padding-lg {
        display: block;
        padding-top: 60px;
        padding-bottom: 60px;
    }

    .practice-area.padding-lg {
        padding-bottom: 55px;
        padding-top: 55px;
    }

    .practice-area .inner {
        border: 1px solid #999999;
        text-align: center;
        margin-bottom: 28px;
        padding: 40px 25px;
    }

    .our-webcoderskull .cnt-block:hover {
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.3);
        border: 0;
    }

    .practice-area .inner h3 {
        color: #3c3c3c;
        font-size: 24px;
        font-weight: 500;
        font-family: 'Poppins', sans-serif;
        padding: 10px 0;
    }

    .practice-area .inner p {
        font-size: 14px;
        line-height: 22px;
        font-weight: 400;
    }

    .practice-area .inner img {
        display: inline-block;
    }


    .our-webcoderskull {
        background: linear-gradient(rgba(246, 247, 249, 0.8), rgba(246, 247, 249, 0.8)), url(https://ppid.tulungagung.bawaslu.go.id/wp-content/uploads/2020/03/background-kpu-2.jpg) no-repeat center center fixed;
        background-size: cover
    }

    .our-webcoderskull .cnt-block {
        float: left;
        width: 100%;
        background: rgb(250, 143, 72);
        padding: 30px 20px;
        text-align: center;
        border: 2px solid #f89d34;
        margin: 0 0 28px;
    }

    .our-webcoderskull .cnt-block figure {
        width: 148px;
        height: 148px;
        border-radius: 100%;
        display: inline-block;
        margin-bottom: 15px;
    }

    .our-webcoderskull .cnt-block img {
        width: 148px;
        height: 148px;
        border-radius: 100%;
    }

    .our-webcoderskull .cnt-block h3 {
        color: #2a2a2a;
        font-size: 20px;
        font-weight: 500;
        padding: 6px 0;
        text-transform: uppercase;
    }

    .our-webcoderskull .cnt-block h3 a {
        text-decoration: none;
        color: #2a2a2a;
    }

    .our-webcoderskull .cnt-block h3 a:hover {
        color: #337ab7;
    }

    .our-webcoderskull .cnt-block p {
        color: #2a2a2a;
        font-size: 13px;
        line-height: 20px;
        font-weight: 400;
    }

    .our-webcoderskull .cnt-block .follow-us {
        margin: 20px 0 0;
    }

    .our-webcoderskull .cnt-block .follow-us li {
        display: inline-block;
        width: auto;
        margin: 0 5px;
    }

    .our-webcoderskull .cnt-block .follow-us li .fa {
        font-size: 24px;
        color: #767676;
    }

    .our-webcoderskull .cnt-block .follow-us li .fa:hover {
        color: #025a8e;
    }
</style>

<body>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <section class="our-webcoderskull padding-lg">
        <div class="container">
            <div class="row heading heading-icon">
                <h2>Pemilihan {{$kategori->deskripsi}}</h2>
            </div>
            <form action="/voting/" method="POST">
                @csrf
                <ul class="row">
                    @foreach($pemilu as $item)
                    <li class="col-12 col-md-6 col-lg-3">
                        <div class="cnt-block equal-hight" style="height: 349px;">
                            <figure><img src="{{asset('data_file/'.$item->foto)}}" class="img-responsive" alt=""></figure>

                            <h3><br>{{$item->nama}}</h3>
                            <ul class="follow-us clearfix">
                                <li>
                                    <input type="hidden" name="pemilu_id" value="{{$item->id}}">
                                    <input type="hidden" name="kategori" value="{{$item->kategori_id}}">
                                    <input class="btn btn-success" type="submit" value="Vote"></i></a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </form>
        </div>
    </section>
    <script>

    </script>
</body>