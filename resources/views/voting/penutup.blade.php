<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Form</title>
</head>

<style type="text/css">
  body {
    margin: 0;
    padding: 0;
    background: linear-gradient(rgba(246, 247, 249, 0.8), rgba(246, 247, 249, 0.8)), url(https://ppid.tulungagung.bawaslu.go.id/wp-content/uploads/2020/03/background-kpu-2.jpg) no-repeat center center fixed;
    background-size: cover;
    height: 110vh;
  }

  #login .container #login-row #login-column #login-box {
    margin-top: 70px;
    max-width: 600px;
    height: 420px;
    border: 1px solid #9C9C9C;
    background-color: #ff8738;
  }

  #login .container #login-row #login-column #login-box #login-form {
    padding: 20px;
  }

  #login .container #login-row #login-column #login-box #login-form #register-link {
    margin-top: -85px;
  }

  .img {
    text-align: center
  }
</style>


<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<body>
  <div id="login">
    <h3 class="text-center text-black pt-5">Terima Kasih Sudah Memilih<br>Pilihan Anda Sangat Berpengaruh Terhadap <br>Masa Depan Bangsa</h3>
    <div class="container">
      <div id="login-row" class="row justify-content-center align-items-center">
        <div id="login-column" class="col-md-6">
          <div id="login-box" class="col-md-12">
            
              <h1 class="text-center text-black">KPU-RI</h1>
              <div class="profile-img">
                <img src="{{asset('KPU.jpg')}}" style="display:block; margin:auto;" alt="profile_img" height="250px" width="250px;">
              </div>
              <div id="register-link" class="text-right">
                <br><br><br><br><a href="{{ route('logout') }}" style="display:block; margin:auto;" class="btn btn-success btn-md" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Kembali ke halaman login</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
              </form>
              </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</body>


</body>

</html>