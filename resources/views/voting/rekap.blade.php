@extends('app')

@section('title')
Edit Profile
@endsection

@section('content')
@foreach ($kategori as $item)
<div class="card card-danger">
    <div class="card-header">
        <h3 class="card-title">{{$item->deskripsi}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <canvas id="pieChart_{{$item->id}}" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
    </div>
    <!-- /.card-body -->
</div>
@endforeach
@endsection

@push('script_chartjs')
<script src="{{asset('admin/plugins/chart.js/Chart.min.js')}}"></script>
<script>
    var test = @json($result);
    alert(JSON.stringify(test));
    var i = 0;
    var arr1 = [];
    var labels1 = [];
    var datas1 = [];

    var arr2 = [];
    var labels2 = [];
    var datas2 = [];

    var arr3 = [];
    var labels3 = [];
    var datas3 = [];

    var arr4 = [];
    var labels4 = [];
    var datas4 = [];

    var arr5 = [];
    var labels5 = [];
    var datas5 = [];

    for (var i = 0; i < test.length; i++) {
        if(test[i]["kategori_id"]==1){
            labels1[i]=test[i]["nama"];
            datas1[i]=test[i]["jumlah_suara"];
            arr1[i]='#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');
        }
        else if(test[i]["kategori_id"]==2){
            labels2[i]=test[i]["nama"];
            datas2[i]=test[i]["jumlah_suara"];
            arr2[i]='#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');
        }
        else if(test[i]["kategori_id"]==3){
            labels3[i]=test[i]["nama"];
            datas3[i]=test[i]["jumlah_suara"];
            arr3[i]='#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');
        }
        else if(test[i]["kategori_id"]==4){
            labels4[i]=test[i]["nama"];
            datas4[i]=test[i]["jumlah_suara"];
            arr4[i]='#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');
        }
        else if(test[i]["kategori_id"]==5){
            labels5[i]=test[i]["nama"];
            datas5[i]=test[i]["jumlah_suara"];
            arr5[i]='#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');
        }
    }

    var donutData1 = {
        labels: labels1,
        datasets: [{
            data: datas1,
            backgroundColor: arr1,
        }]
    }

    var donutData2 = {
        labels: labels2,
        datasets: [{
            data: datas2,
            backgroundColor: arr2,
        }]
    }

    var donutData3 = {
        labels: labels3,
        datasets: [{
            data: datas3,
            backgroundColor: arr3,
        }]
    }

    var donutData4 = {
        labels: labels4,
        datasets: [{
            data: datas4,
            backgroundColor: arr4,
        }]
    }

    var donutData5 = {
        labels: labels5,
        datasets: [{
            data: datas5,
            backgroundColor: arr5,
        }]
    }

    var pieChartCanvas1 = $('#pieChart_1').get(0).getContext('2d');
    var pieChartCanvas2 = $('#pieChart_2').get(0).getContext('2d');
    var pieChartCanvas3 = $('#pieChart_3').get(0).getContext('2d');
    var pieChartCanvas4 = $('#pieChart_4').get(0).getContext('2d');
    var pieChartCanvas5 = $('#pieChart_5').get(0).getContext('2d');
    var pieData1 = donutData1; var pieData2 = donutData2; var pieData3 = donutData3; var pieData4 = donutData4; var pieData5 = donutData5;
    var pieOptions = {
        maintainAspectRatio: false,
        responsive: true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
   var chart1 = new Chart(pieChartCanvas1, {
        type: 'pie',
        data: pieData1,
        options: pieOptions
    });
    var chart2 = new Chart(pieChartCanvas2, {
        type: 'pie',
        data: pieData2,
        options: pieOptions
    });
    var chart3 = new Chart(pieChartCanvas3, {
        type: 'pie',
        data: pieData3,
        options: pieOptions
    });
    var chart4 = new Chart(pieChartCanvas4, {
        type: 'pie',
        data: pieData4,
        options: pieOptions
    });
    var chart5 = new Chart(pieChartCanvas5, {
        type: 'pie',
        data: pieData5,
        options: pieOptions
    });
</script>
@endpush