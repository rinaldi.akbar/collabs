@extends('app')

@section('title')
Edit Profile
@endsection

@section('content')
@foreach ($kategori as $item)
<div class="card card-danger">
    <div class="card-header">
        <h3 class="card-title">{{$item->deskripsi}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <canvas id="barChart_{{$item->id}}" width="800" height="450"></canvas>
    </div>
    <!-- /.card-body -->
</div>
@endforeach
@endsection

@push('script_chartjs')
<script src="{{asset('admin/plugins/chart.js/Chart.min.js')}}"></script>
<script>
    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    
    var test = @json($result);
    var i = 0;
    var arr1 = [];
    var labels1 = [];
    var datas1 = [];

    var arr2 = [];
    var labels2 = [];
    var datas2 = [];

    var arr3 = [];
    var labels3 = [];
    var datas3 = [];

    var arr4 = [];
    var labels4 = [];
    var datas4 = [];

    var arr5 = [];
    var labels5 = [];
    var datas5 = [];

    for (var i = 0; i < test.length; i++) {
        if (test[i]["kategori_id"] == 1) {
            labels1.push(test[i]["nama"]);
            datas1.push(test[i]["jumlah_suara"]);
            arr1.push('#'+Math.floor(Math.random()*16777215).toString(16));
        } else if (test[i]["kategori_id"] == 2) {
            labels2.push(test[i]["nama"]);
            datas2.push(test[i]["jumlah_suara"]);
            arr2.push('#'+Math.floor(Math.random()*16777215).toString(16));
        } else if (test[i]["kategori_id"] == 3) {
            labels3.push(test[i]["nama"]);
            datas3.push(test[i]["jumlah_suara"]);
            arr3.push('#'+Math.floor(Math.random()*16777215).toString(16));
        } else if (test[i]["kategori_id"] == 4) {
            labels4.push(test[i]["nama"]);
            datas4.push(test[i]["jumlah_suara"]);
            arr4.push('#'+Math.floor(Math.random()*16777215).toString(16));
        } else {
            labels5.push(test[i]["nama"]);
            datas5.push(test[i]["jumlah_suara"]);
            arr5.push('#'+Math.floor(Math.random()*16777215).toString(16));
        }
    }

    new Chart(document.getElementById("barChart_1"), {
        type: 'bar',
        data: {
            labels: labels1,
            datasets: [{
                label: "Jumlah Suara",
                backgroundColor: arr1,
                data: datas1
            }]
        },
        options: {
            legend: {
                display: false
            },
            title: {
                display: true,
                text: 'Pemilihan Calon Gubernur dan Wakil Gubernur'
            }
        }
    });

    new Chart(document.getElementById("barChart_2"), {
        type: 'bar',
        data: {
            labels: labels2,
            datasets: [{
                label: "Jumlah Suara",
                backgroundColor: arr2,
                data: datas2
            }]
        },
        options: {
            legend: {
                display: false
            },
            title: {
                display: true,
                text: 'Pemilihan Calon Anggota DPRD'
            }
        }
    });

    new Chart(document.getElementById("barChart_3"), {
        type: 'bar',
        data: {
            labels: labels3,
            datasets: [{
                label: "Jumlah Suara",
                backgroundColor: arr3,
                data: datas3
            }]
        },
        options: {
            legend: {
                display: false
            },
            title: {
                display: true,
                text: 'Pemilihan Calon Anggota DPRD'
            }
        }
    });

    new Chart(document.getElementById("barChart_4"), {
        type: 'bar',
        data: {
            labels: labels4,
            datasets: [{
                label: "Jumlah Suara",
                backgroundColor: arr4,
                data: datas4
            }]
        },
        options: {
            legend: {
                display: false
            },
            title: {
                display: true,
                text: 'Pemilihan Calon Anggota DPRD'
            }
        }
    });

    new Chart(document.getElementById("barChart_5"), {
        type: 'bar',
        data: {
            labels: labels5,
            datasets: [{
                label: "Jumlah Suara",
                backgroundColor: arr5,
                data: datas5
            }]
        },
        options: {
            legend: {
                display: false
            },
            title: {
                display: true,
                text: 'Pemilihan Calon Anggota DPRD'
            }
        }
    });
</script>
@endpush