<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware('auth')->group(function () {
Route::resource('kategori', KategoriController::class)->only([
    'index','create','store','edit','update','destroy'
]);
Route::resource('pemilu', PemiluController::class)->only([
    'index','create','store','edit','update','destroy'
]);
Route::resource('profile', ProfileController::class)->only([
    'index','edit','update','create'
]);
Route::get('/voting/{id}/vote', 'VotingController@vote');
Route::get('/voting/rekapitulasi', 'VotingController@rekapitulasi');
Route::post('/voting', 'VotingController@store');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register/create', 'RegisterController@create');